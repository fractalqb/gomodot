package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"

	"golang.org/x/mod/semver"
)

var highlight = "bisque"

type pkg struct {
	pid      int
	name     string
	useno    int
	versions []*pkgversion
}

type pkgversion struct {
	nid  int
	tag  string
	used bool
	pkg  *pkg
	deps []*pkgversion
}

func (d *pkgversion) addDep(s *pkgversion) {
	for _, v := range d.deps {
		if sameVersion(v, s) {
			return
		}
	}
	d.deps = append(d.deps, s)
}

func sameVersion(p1, p2 *pkgversion) bool {
	return p1.pkg == p2.pkg && p1.tag == p2.tag
}

var (
	packages = make(map[string]*pkg)
	versions []*pkgversion
)

func getPkgVersion(pkgstr string) *pkgversion {
	sep := strings.IndexByte(pkgstr, '@')
	var name, tag string
	if sep < 0 {
		name = pkgstr
	} else {
		name = pkgstr[:sep]
		tag = pkgstr[sep+1:]
	}
	p := packages[name]
	if p == nil {
		p = &pkg{pid: len(packages), name: name}
		v := &pkgversion{tag: tag, pkg: p, nid: len(versions)}
		p.versions = []*pkgversion{v}
		versions = append(versions, v)
		packages[name] = p
		return v
	}
	for _, v := range p.versions {
		if v.tag == tag {
			return v
		}
	}
	v := &pkgversion{tag: tag, pkg: p, nid: len(versions)}
	versions = append(versions, v)
	p.versions = append(p.versions, v)
	return v
}

func readGraph(rd io.Reader) {
	scn := bufio.NewScanner(rd)
	for scn.Scan() {
		f := strings.Split(scn.Text(), " ")
		d := getPkgVersion(f[0])
		s := getPkgVersion(f[1])
		d.addDep(s)
	}
	for _, p := range packages {
		sort.Slice(p.versions, func(i, j int) bool {
			vi, vj := p.versions[i], p.versions[j]
			return semver.Compare(vi.tag, vj.tag) > 0
		})
		mayUse := len(p.versions) - 1
		for i := mayUse; i > 0; i-- {
			v0, v1 := p.versions[i], p.versions[i-1]
			if semver.Major(v0.tag) != semver.Major(v1.tag) {
				p.versions[mayUse].used = true
				p.useno++
			}
			mayUse = i - 1
		}
		p.versions[mayUse].used = true
		p.useno++
	}
}

func writeDot(wr io.Writer) {
	fmt.Fprintln(wr, "digraph gomod {")
	fmt.Fprintf(wr, "\trankdir=%s;\n", rankdir)
	switch details {
	case "p":
		writePkgsOnly(wr)
	case "pv":
		writePkgVersions(wr)
	case "v":
		writeVersions(wr)
	default:
		log.Fatalf("Unknown detail level: '%s'", details)
	}
	fmt.Fprintln(wr, "}")
}

func writePkgsOnly(wr io.Writer) {
	fmt.Fprintln(wr, "\tnode [shape=box];")
	for _, p := range packages {
		if p.useno > 1 {
			fmt.Fprintf(wr, "\t%d [label=\"%s\",fillcolor=%s,style=filled];\n", p.pid, p.name, highlight)
		} else {
			fmt.Fprintf(wr, "\t%d [label=\"%s\"];\n", p.pid, p.name)
		}
	}
	for _, p := range packages {
		dmap := make(map[int]*pkg)
		for _, dv := range p.versions {
			for _, sv := range dv.deps {
				dmap[sv.pkg.pid] = sv.pkg
			}
		}
		for pid := range dmap {
			fmt.Printf("\t%d -> %d;\n", p.pid, pid)
		}
	}
}

func writePkgVersions(wr io.Writer) {
	const lbstart = ``
	fmt.Fprintln(wr, "\tnode [shape=none];")
	for _, p := range packages {
		form := `	%d [label=<<TABLE border="0" cellborder="1" cellspacing="0" cellpadding="8"><TR><TD>%s</TD></TR><TR><TD>`
		if p.useno > 1 {
			form = `	%d [label=<<TABLE border="0" cellborder="1" cellspacing="0" cellpadding="8"><TR><TD bgcolor="` +
				highlight +
				`">%s</TD></TR><TR><TD>`
		}
		fmt.Fprintf(wr, form, p.pid, p.name)
		if len(p.versions) == 1 {
			fmt.Fprint(wr, p.versions[0].tag)
		} else {
			for i, v := range p.versions {
				if i > 0 {
					io.WriteString(wr, "<BR/>")
				}
				if v.used {
					fmt.Fprintf(wr, "<B>%s</B>", v.tag)
				} else {
					fmt.Fprintf(wr, "%s", v.tag)
				}
			}
		}
		fmt.Fprintln(wr, "</TD></TR></TABLE>>];")
	}
	for _, p := range packages {
		dmap := make(map[int]*pkg)
		for _, dv := range p.versions {
			for _, sv := range dv.deps {
				dmap[sv.pkg.pid] = sv.pkg
			}
		}
		for pid := range dmap {
			fmt.Printf("\t%d -> %d;\n", p.pid, pid)
		}
	}
}

func writeVersions(wr io.Writer) {
	fmt.Fprintln(wr, "\tnode [shape=box,color=gray];")
	clutserNo := 0
	for _, p := range packages {
		if len(p.versions) == 1 {
			v := p.versions[0]
			if v.tag == "" {
				fmt.Fprintf(wr, "\t%d [label=\"%s\"];\n", v.nid, p.name)
			} else {
				fmt.Fprintf(wr, "\t%d [label=\"%s\\n%s\"];\n", v.nid, p.name, v.tag)
			}
		} else {
			clutserNo++
			fmt.Fprintf(wr, "\tsubgraph cluster_%d {\n", clutserNo)
			fmt.Fprintf(wr, "\t\tlabel=\"%s\";\n", p.name)
			fmt.Fprintln(wr, "\t\tcolor=gray;")
			if p.useno > 1 {
				fmt.Fprintf(wr, "\t\tfillcolor=%s;\n", highlight)
				fmt.Fprintln(wr, "\t\tstyle=filled;")
			}
			for _, v := range p.versions {
				if v.used {
					fmt.Fprintf(wr, "\t\t%d [label=\"%s\",shape=none];\n",
						v.nid,
						v.tag)
				} else {
					fmt.Fprintf(wr, "\t\t%d [label=\"%s\",shape=none,fontcolor=gray];\n",
						v.nid,
						v.tag)
				}
			}
			fmt.Fprintln(wr, "\t}")
		}
	}

	for _, d := range versions {
		for _, s := range d.deps {
			fmt.Fprintf(wr, "\t%d -> %d;\n", d.nid, s.nid)
		}
	}
}

var (
	details string = "pv"
	rankdir string = "LR"
)

func main() {
	flag.StringVar(&details, "d", details, `Detail level: p, pv, v
- p:  Show dependencies between packages only, no version
      details
- pv: Show dependencies between packages only and list all
      involved versions
- v:  Shows dependencies between package versions`)
	flag.StringVar(&rankdir, "rankdir", rankdir, "Set graphs rankdir (see graphviz doc)")
	flag.StringVar(&highlight, "h", highlight, "Set highlight color")
	flag.Parse()
	readGraph(os.Stdin)
	writeDot(os.Stdout)
}
