module git.fractalqb.de/fractalqb/gomodot

go 1.16

require (
	golang.org/x/mod v0.7.0
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
)
