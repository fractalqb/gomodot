# gomodot

Convert output from go mod graph into a graphviz dot file

Unlike modgraphviz, gomodot packs different versions of a package together into a Graphviz subgraph cluster.

